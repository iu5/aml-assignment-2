FROM nvcr.io/nvidia/pytorch:21.06-py3

ENV DEBIAN_FRONTEND noninteractive
ENV TORCH_CUDA_ARCH_LIST="6.0 6.1 7.0+PTX 8.0 8.6"
ENV TORCH_NVCC_FLAGS="-Xfatbin -compress-all"
ENV CMAKE_PREFIX_PATH="$(dirname $(which conda))/../"

RUN apt-get update && apt-get install -y \
    ffmpeg libsm6 libxext6 git ninja-build libglib2.0-0 libsm6 libxrender-dev libxext6 \
    ca-certificates wget sudo \
    tmux nano vim \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install MMCV
RUN git clone --branch v1.3.16 https://github.com/open-mmlab/mmcv.git /mmcv
WORKDIR /mmcv
RUN CUDA_HOME=/usr/local/cuda FORCE_CUDA=1 MMCV_WITH_OPS=1 pip install -e .

# Install MMDetection
RUN conda clean --all
RUN git clone --branch v2.18.0 https://github.com/open-mmlab/mmdetection.git /mmdetection
WORKDIR /mmdetection
ENV FORCE_CUDA="1"
RUN pip install -r requirements/build.txt
RUN pip install --no-cache-dir -e .

# install MMSegmentation
RUN git clone --branch v0.18.0 https://github.com/open-mmlab/mmsegmentation.git /mmsegmentation
RUN pip install -e /mmsegmentation

# Install damage detection requirements
WORKDIR /root
COPY requirements/train.txt requirements_train.txt
RUN pip install -r requirements_train.txt

# create a non-root user
ARG USER_ID=1000
RUN useradd -m --no-log-init --system  --uid ${USER_ID} appuser -g sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
USER appuser

WORKDIR /home/appuser
ENV PATH="/home/appuser/.local/bin:${PATH}"