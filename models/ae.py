import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as transforms
from torch.utils.data import TensorDataset, DataLoader
from matplotlib import pyplot as plt
from tqdm import tqdm

class autoencoder(nn.Module):
    def __init__(self, input_size, latent_dim):
        super(autoencoder, self).__init__()
        # Step 1 : Define the encoder 
        # Step 2 : Define the decoder
        # Step 3 : Initialize the weights (optional)

        self.encoder = nn.Sequential(
          nn.Linear(input_size, inputч_size//2),
          nn.ReLU(True),
          nn.Linear(input_size//2, input_size//3),
          nn.Linear(input_size//3, input_size//4),
          nn.Tanh(),
          nn.Linear(input_size//4, latent_dim)
      )
    
        self.decoder = nn.Sequential(
          nn.Linear(latent_dim, input_size//4),
          nn.ReLU(True),
          nn.Linear(input_size//4, input_size//3),
          nn.Linear(input_size//3, input_size//2),
          nn.Tanh(),
          nn.Linear(input_size//2, input_size)
      )
        
        self.encoder.apply(self.__init_weights)
        self.decoder.apply(self.__init_weights)
        
    def forward(self, x, indicator):
        # Step 1: Pass the input through encoder to get latent representation
        # Step 2: Take latent representation and pass through decoder
#         print(x.dtype)
#         print(indicator.dtype)
#         print(x.shape)
#         print(indicator.shape)
        x = self.encoder(x * indicator)
        x = self.decoder(x)
        return x
        

    def encode(self,input):
        #Step 1: Pass the input through the encoder to get latent representation
        return self.encoder(input)
    
    def __init_weights(self,m):
        #Init the weights (optional)
        if type(m) == nn.Linear:
            torch.nn.init.xavier_uniform_(m.weight)
            m.bias.data.fill_(0.01)


# VAE model
class VAE(nn.Module):
    def __init__(self, input_size=input_size, h_dim=400, z_dim=40):
        super(VAE, self).__init__()
        self.fc1 = nn.Linear(input_size, h_dim)
        self.fc2 = nn.Linear(h_dim, z_dim)
        self.fc3 = nn.Linear(h_dim, z_dim)
        self.fc4 = nn.Linear(z_dim, h_dim)
        self.fc5 = nn.Linear(h_dim, input_size)
        
    def encode(self, x):
        h = F.relu(self.fc1(x))
        return self.fc2(h), self.fc3(h)
    
    def reparameterize(self, mu, log_var):
        std = torch.exp(log_var/2)
        eps = torch.randn_like(std)
        return mu + eps * std

    def decode(self, z):
        h = F.relu(self.fc4(z))
        return F.sigmoid(self.fc5(h))
    
    def forward(self, x, indicator):
        mu, log_var = self.encode(x * indicator)
        z = self.reparameterize(mu, log_var)
        x_reconst = self.decode(z)
        return x_reconst, mu, log_var