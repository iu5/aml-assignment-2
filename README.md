
```
.
├── Dockerfile # dockerfile 
├── README.md  # documentation
├── models # models source code 
│   ├── ae.py # autoencoder code
│   └── gan.py # gan code
├── notebooks # notebooks with demo 
│   ├── task_1.ipynb # solution of task1
│   └── task_2.ipynb # solution of task2
└── requirements.txt # all python dependencies
```


### Build image

```
docker build -t ml_cuda .
```

### Run container
```
docker run ml_cuda:latest
```

### Go into container
```
docker exec -it ml_cuda:latest
```

### Install dependencies
```
pip install -r requirements.txt
```